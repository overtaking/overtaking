package fr.campus.overtaking.inject;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Component;
import fr.campus.overtaking.MainActivity;
import fr.campus.overtaking.dao.CategoryDao;
import fr.campus.overtaking.db.Overtaking;
import fr.campus.overtaking.service.category.CategoryService;

@Singleton
@Component(modules = {AppModuleApplication.class,AppModule.class})
public interface AppComponent {

    CategoryDao categoryDao();

    Overtaking overtaking();

    CategoryService categoryServiceImpl();

    Application application();

}
