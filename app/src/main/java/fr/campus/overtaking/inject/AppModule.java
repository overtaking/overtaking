package fr.campus.overtaking.inject;

import android.app.Application;

import androidx.room.Room;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import fr.campus.overtaking.dao.CategoryDao;
import fr.campus.overtaking.db.Overtaking;
import fr.campus.overtaking.service.category.CategoryService;
import fr.campus.overtaking.impl.CategoryServiceImpl;

@Module
public class AppModule {

    private Overtaking overtaking;

    public AppModule(Application application){
        overtaking = Room.databaseBuilder(application,Overtaking.class,"overtaking-db").build();
    }

    @Singleton
    @Provides
    Overtaking overtaking(){
        return overtaking;
    }

    @Singleton
    @Provides
    CategoryDao getCategoryDao(Overtaking overtaking){
        return overtaking.getCategoryDao();
    }

    @Singleton
    @Provides
    CategoryService categoryServiceImpl(CategoryDao categoryDao){
       return new CategoryServiceImpl(categoryDao);
    }
}
