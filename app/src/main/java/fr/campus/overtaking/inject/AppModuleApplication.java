package fr.campus.overtaking.inject;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModuleApplication {

    Application application;

    public AppModuleApplication(Application application){
        application = application;
    }

    @Provides
    @Singleton
    Application providesApplication(){
        return application;
    }
}
