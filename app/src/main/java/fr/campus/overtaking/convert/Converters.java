package fr.campus.overtaking.convert;

import androidx.room.TypeConverter;

import java.util.Date;

/**
 * @Author Pascal Bouchez && Vassili BERNARD
 * @Date 11/02/2020 15:21
 *
 * Converti les timestamp en date pour la base de données
 */
public class Converters {
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}
