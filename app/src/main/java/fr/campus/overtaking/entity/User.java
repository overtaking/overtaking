package fr.campus.overtaking.entity;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.Relation;
import androidx.room.TypeConverters;

import java.util.Date;
import java.util.List;

import fr.campus.overtaking.convert.Converters;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 09:54
 *
 * Entité utilisateur
 *
 */
@Getter
@Setter
@ToString
@Entity
public class User {

    @PrimaryKey
    private Long idUser;
    private String lastName;
    private String firstName;
    private String password;
    private String email;

    @TypeConverters({Converters.class})
    private Date dateM;

    @TypeConverters({Converters.class})
    private Date dateC;
}
