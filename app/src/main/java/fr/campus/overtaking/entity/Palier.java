package fr.campus.overtaking.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 10:56
 *
 * Entité Palier
 */
@Entity
@Getter
@Setter
@ToString
public class Palier {

    @PrimaryKey
    private Long idPalier;
    private String libellePalier;
}
