package fr.campus.overtaking.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 09:54
 *
 * Entité Category
 *
 */
@Entity
@Getter
@Setter
@ToString
public class Category {

    @PrimaryKey
    private Long idCategory;
    private String Libelle;

    public Category() {
    }

    public Category(Long idCategory, String libelle) {
        this.idCategory = idCategory;
        Libelle = libelle;
    }
}
