package fr.campus.overtaking.entity;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 11/02/2020 15:32
 *
 * Entité associative UserWithChallenge
 *
 */
@Getter
@Setter
@ToString
public class UserWithChallenge {
    @Embedded
    private User user;

    @Relation(parentColumn = "idUser",entityColumn = "idChallenge",associateBy = @Junction(Realiser.class))
    private List<Challenge> challenges;
}
