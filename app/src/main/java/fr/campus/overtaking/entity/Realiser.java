package fr.campus.overtaking.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.Relation;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 11/02/2020 15:56
 *
 * Classe d'association Realiser
 */
@Entity(primaryKeys = {"idChallenge","idUser"})
@Getter
@Setter
@ToString
public class Realiser {

    @NonNull
    private Long idChallenge;
    @NonNull
    private Long idUser;

    private Boolean isValid;
}
