package fr.campus.overtaking.entity;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.Relation;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 09:54
 *
 * Entité Challenge
 *
 */
@Getter
@Setter
@ToString
@Entity
public class Challenge {

    @PrimaryKey
    private Long idChallenge;
    private String libelle;
    private Integer qte;

//    @Relation(entity = Category.class,entityColumn = "idCategory",parentColumn = "idChallenge")
//    private Category category;
//
//    @Relation(entity = Palier.class,entityColumn = "idPalier",parentColumn = "idChallenge")
//    private Palier palier;
}
