package fr.campus.overtaking.entity;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 11/02/2020 15:58
 *
 * Classe d'association ChallengeWithUser
 */
@Getter
@Setter
@ToString
public class ChallengeWithUser {

    @Embedded
    private Challenge challenge;

    @Relation(parentColumn = "idChallenge",entityColumn = "idUser",associateBy = @Junction(Realiser.class))
    private List<User> users;
}
