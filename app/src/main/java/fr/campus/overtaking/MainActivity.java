package fr.campus.overtaking;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;

import fr.campus.overtaking.db.Overtaking;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Overtaking db = Room.databaseBuilder(getApplicationContext(),
                Overtaking.class, "overtaking").build();

    }
}
