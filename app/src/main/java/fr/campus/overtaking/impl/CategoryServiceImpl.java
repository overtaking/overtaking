package fr.campus.overtaking.impl;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import fr.campus.overtaking.dao.CategoryDao;
import fr.campus.overtaking.entity.Category;
import fr.campus.overtaking.service.category.CategoryService;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 15:42
 *
 * Classe de service CategoryServiceImpl
 */
public class CategoryServiceImpl implements CategoryService {

    private CategoryDao categoryDao;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Inject
    public CategoryServiceImpl(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }


    @Override
    public void create(Category object) {
        logger.log(Level.ALL,"Insert into database {}",object.getIdCategory());
        categoryDao.insert(object);
    }

    @Override
    public void update(Category object) {
        logger.log(Level.ALL,"Update into database {}",object.getIdCategory());
        categoryDao.update(object);
    }

    @Override
    public void delete(Category object) {
        logger.log(Level.ALL,"Delete into database {}",object.getIdCategory());
        categoryDao.delete(object);
    }

    @Override
    public List<Category> findAll() {
        logger.log(Level.ALL,"Find all {}",this.getClass().getName());
        return categoryDao.findAll();
    }

    @Override
    public Category findByIds(long[] ids) {
        logger.log(Level.ALL,"Find by ids {}",ids);
        return categoryDao.findByIds(ids);
    }
}
