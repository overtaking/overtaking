package fr.campus.overtaking.service.palier.impl;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import dagger.Component;
import dagger.Module;
import fr.campus.overtaking.dao.PalierDao;
import fr.campus.overtaking.entity.Palier;
import fr.campus.overtaking.service.palier.PalierService;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 15:42
 *
 * Classe de service PalierServiceImpl
 */
public class PalierServiceImpl implements PalierService {

    private final PalierDao palierDao;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Inject
    public PalierServiceImpl(PalierDao palierDao) {
        this.palierDao = palierDao;
    }

    @Override
    public void create(Palier object) {
        logger.log(Level.ALL,"Insert into database {}",object.getIdPalier());
        palierDao.insert(object);
    }

    @Override
    public void update(Palier object) {
        logger.log(Level.ALL,"Update into database {}",object.getIdPalier());
        palierDao.update(object);
    }

    @Override
    public void delete(Palier object) {
        logger.log(Level.ALL,"Delete into database {}",object.getIdPalier());
        palierDao.delete(object);
    }

    @Override
    public List<Palier> findAll() {
        logger.log(Level.ALL,"Find all {}",this.getClass().getName());
        return palierDao.findAll();
    }

    @Override
    public Palier findByIds(long[] ids) {
        logger.log(Level.ALL,"Find by ids {}",ids);
        return palierDao.findByIds(ids);
    }
}
