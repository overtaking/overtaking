package fr.campus.overtaking.service.category;

import fr.campus.overtaking.entity.Category;
import fr.campus.overtaking.service.Service;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 15:42
 *
 * Interface de service CategoryService
 */
public interface CategoryService extends Service<Category> {
}
