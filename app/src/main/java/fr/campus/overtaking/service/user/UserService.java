package fr.campus.overtaking.service.user;

import fr.campus.overtaking.entity.User;
import fr.campus.overtaking.service.Service;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 15:42
 *
 * Interface de service UserService
 */
public interface UserService extends Service<User> {
}
