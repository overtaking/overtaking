package fr.campus.overtaking.service.user.impl;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import dagger.Component;
import dagger.Module;
import fr.campus.overtaking.dao.UserDao;
import fr.campus.overtaking.entity.User;
import fr.campus.overtaking.service.user.UserService;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 15:42
 *
 * Classe de service UserServiceImpl
 */
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Inject
    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void create(User object) {
        logger.log(Level.ALL,"Insert into database {}",object.getIdUser());
        userDao.insert(object);
    }

    @Override
    public void update(User object) {
        logger.log(Level.ALL,"Update into database {}",object.getIdUser());
        userDao.update(object);
    }

    @Override
    public void delete(User object) {
        logger.log(Level.ALL,"Delete into database {}",object.getIdUser());
        userDao.delete(object);
    }

    @Override
    public List<User> findAll() {
        logger.log(Level.ALL,"Find all {}",this.getClass().getName());
        return userDao.findAll();
    }

    @Override
    public User findByIds(long[] ids) {
        logger.log(Level.ALL,"Find by ids {}",ids);
        return userDao.findByIds(ids);
    }
}
