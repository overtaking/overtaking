package fr.campus.overtaking.service.challenge.impl;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import dagger.Component;
import dagger.Module;
import fr.campus.overtaking.dao.ChallengeDao;
import fr.campus.overtaking.entity.Challenge;
import fr.campus.overtaking.service.challenge.ChallengeService;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 15:42
 *
 * Classe de service ChallengeServiceImpl
 */
public class ChallengeServiceImpl implements ChallengeService {

    private final ChallengeDao challengeDao;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Inject
    public ChallengeServiceImpl(ChallengeDao challengeDao) {
        this.challengeDao = challengeDao;
    }


    @Override
    public void create(Challenge object) {
        logger.log(Level.ALL,"Insert into database {}",object.getIdChallenge());
        challengeDao.insert(object);
    }

    @Override
    public void update(Challenge object) {
        logger.log(Level.ALL,"Update into database {}",object.getIdChallenge());
        challengeDao.update(object);
    }

    @Override
    public void delete(Challenge object) {
        logger.log(Level.ALL,"Delete into database {}",object.getIdChallenge());
        challengeDao.delete(object);
    }

    @Override
    public List<Challenge> findAll() {
        logger.log(Level.ALL,"Find all {}",this.getClass().getName());
        return challengeDao.findAll();
    }

    @Override
    public Challenge findByIds(long[] ids) {
        logger.log(Level.ALL,"Find by ids {}",ids);
        return challengeDao.findByIds(ids);
    }
}
