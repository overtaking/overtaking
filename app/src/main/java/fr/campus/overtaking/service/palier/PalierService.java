package fr.campus.overtaking.service.palier;

import fr.campus.overtaking.entity.Palier;
import fr.campus.overtaking.service.Service;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 15:42
 *
 * Interface de service PalierService
 */
public interface PalierService extends Service<Palier> {
}
