package fr.campus.overtaking.service.challenge;

import fr.campus.overtaking.entity.Challenge;
import fr.campus.overtaking.service.Service;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 15:42
 *
 * Interface de service ChallengeService
 */
public interface ChallengeService extends Service<Challenge> {
}
