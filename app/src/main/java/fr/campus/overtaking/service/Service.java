package fr.campus.overtaking.service;

import java.util.List;

/**
 * @Author Vassili BERNARD
 * @Date 10/02/2020 15:45
 *
 * Interface de service générique
 *
 * @param <T>
 */
public interface Service<T> {

    void create(T object);
    void update(T object);
    void delete(T object);
    List<T> findAll();
    T findByIds(long[] ids);
}
