package fr.campus.overtaking.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 13:47
 *
 * Dao generique
 * Contient create,update,delete
 * @param <T>
 */
@Dao
public interface CrudDao <T> {

    @Insert
    Long insert(T object);

    @Update
    void update(T object);

    @Delete
    void delete(T object);
}
