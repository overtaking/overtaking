package fr.campus.overtaking.dao;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import fr.campus.overtaking.entity.Category;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 14:00
 *
 * Dao category
 */
@Dao
public interface CategoryDao extends CrudDao<Category> {

    @Query("SELECT * FROM Category")
    List<Category> findAll();

    @Query("SELECT * FROM Category where idCategory IN (:id)")
    Category findByIds(long [] id);
}
