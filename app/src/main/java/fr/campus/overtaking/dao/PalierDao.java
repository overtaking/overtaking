package fr.campus.overtaking.dao;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import fr.campus.overtaking.entity.Palier;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 14:00
 *
 * Dao Palier
 */
@Dao
public interface PalierDao extends CrudDao<Palier> {

    @Query("SELECT * FROM Palier")
    List<Palier> findAll();

    @Query("SELECT * FROM Palier where idPalier IN (:id)")
    Palier findByIds(long[] id);
}
