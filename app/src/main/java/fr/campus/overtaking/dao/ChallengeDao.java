package fr.campus.overtaking.dao;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import fr.campus.overtaking.entity.Challenge;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 14:00
 *
 * Dao Challenge
 */
@Dao
public interface ChallengeDao extends CrudDao<Challenge> {

    @Query("SELECT * FROM Challenge")
    List<Challenge> findAll();

    @Query("SELECT * FROM Challenge where idChallenge IN (:id)")
    Challenge findByIds(long[] id);
}
