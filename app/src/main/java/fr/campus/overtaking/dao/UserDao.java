package fr.campus.overtaking.dao;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import fr.campus.overtaking.entity.User;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 14:00
 *
 * Dao User
 */
@Dao
public interface UserDao extends CrudDao<User> {

    @Query("SELECT * FROM User")
    List<User> findAll();

    @Query("SELECT * FROM User where idUser IN (:id)")
    User findByIds(long[] id);
}
