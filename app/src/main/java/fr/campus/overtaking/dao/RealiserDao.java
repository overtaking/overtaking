package fr.campus.overtaking.dao;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import fr.campus.overtaking.entity.ChallengeWithUser;
import fr.campus.overtaking.entity.Realiser;
import fr.campus.overtaking.entity.UserWithChallenge;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 14:00
 *
 * Dao Realiser
 */
@Dao
public interface RealiserDao extends CrudDao<Realiser> {

    @Transaction
    @Query("SELECT * FROM Challenge")
    List<ChallengeWithUser> getChallengeWithUser();

    @Transaction
    @Query("SELECT * FROM User")
    List<UserWithChallenge> getUserWithChallenge();
}
