package fr.campus.overtaking.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import fr.campus.overtaking.dao.CategoryDao;
import fr.campus.overtaking.dao.ChallengeDao;
import fr.campus.overtaking.dao.PalierDao;
import fr.campus.overtaking.dao.RealiserDao;
import fr.campus.overtaking.dao.UserDao;
import fr.campus.overtaking.entity.Category;
import fr.campus.overtaking.entity.Challenge;
import fr.campus.overtaking.entity.Palier;
import fr.campus.overtaking.entity.Realiser;
import fr.campus.overtaking.entity.User;

/**
 * @Author Vassili BERNARD && Pascal BOUCHEZ
 * @Date 10/02/2020 14:26
 *
 * Classe asbtraite Overtaking
 *
 * Cette classes décrit la base de données
 *
 * Ceci est essentiel pour la persistance avec Android Room
 */
@Database(entities = {Category.class, Challenge.class, Palier.class, Realiser.class, User.class},version = 1,exportSchema = false)
public abstract class Overtaking extends RoomDatabase {
    public abstract CategoryDao getCategoryDao();
    public abstract ChallengeDao getChallengeDao();
    public abstract PalierDao getPalierDao();
    public abstract RealiserDao getRealiserDao();
    public abstract UserDao getUserDao();
}
