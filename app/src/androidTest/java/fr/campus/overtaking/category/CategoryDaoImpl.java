package fr.campus.overtaking.category;

import android.content.Context;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.List;

import fr.campus.overtaking.dao.CategoryDao;
import fr.campus.overtaking.db.Overtaking;
import fr.campus.overtaking.entity.Category;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class CategoryDaoImpl {

    private CategoryDao categoryDao;
    private Overtaking db;

    @Before
    public void init(){
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, Overtaking.class).build();
        categoryDao = db.getCategoryDao();
    }

    @After
    public void close(){
        db.close();
    }

    @Test
    public void insert_catogory(){
        String libelle = "Course";
        Category category = new Category();
        category.setLibelle(libelle);

        long idCategory = categoryDao.insert(category);
        Category course = categoryDao.findByIds(new long[]{idCategory});

        assertNotNull(course);
        assertEquals(libelle,course.getLibelle());
    }

    @Test
    public void delete_category(){
        Category category = new Category();
        category.setLibelle("Marathon");

        long idCategory = categoryDao.insert(category);
        List<Category> categories = categoryDao.findAll();
        assertEquals(1,categories.size());

        categoryDao.delete(category);
        categories = categoryDao.findAll();

        assertEquals(0,categories.size());
    }

    @Test
    public void update_category(){
        Category category = new Category();
        category.setLibelle("Marathon");

        Long idCategory = categoryDao.insert(category);
        category = categoryDao.findByIds(new long[]{idCategory});

        assertNotNull(category.getIdCategory());
        assertNotNull(category.getLibelle());
        assertEquals("Marathon",category.getLibelle());

        category.setLibelle("Course");
        categoryDao.update(category);

        category = categoryDao.findByIds(new long[]{idCategory});
        assertNotNull(category.getIdCategory());
        assertEquals("Course",category.getLibelle());

    }

}
